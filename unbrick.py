__author__ = 'moritz'

import sys
import time
import serial

XMODEM_NAK = 0x15

def start_bootloader(fd):
    try:
        while s.read() != bytes([XMODEM_NAK]):
            s.write(b"\xBB\x11\x22\x33\x44\x55\x66\x77")
    except serial.SerialException as e:
        print("Serial port died. That can happen (hardware and stuff...). Try again.")
        sys.exit(1)
    return True

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: %s <port> <uimage>" % sys.argv[0])
        sys.exit(1)

    s = serial.Serial(sys.argv[1], 115200, timeout=0.1)

    print("Power your NAS now.")
    print("Waiting...")

    start_bootloader(s)

    print("Success - sending boot image...")

    s.close()

